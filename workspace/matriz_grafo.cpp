//#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <fstream>
using namespace std;

#define HIGH 999999

#include "matriz_grafo.h"

matriz_grafo::matriz_grafo(){}

/* ------------------------------------------------------------ */
/* --- Funciones para manipular los nombres de los vertices --- */
/* ------------------------------------------------------------ */

/* Función para asignar nombres a las aristas */
void matriz_grafo::agregar_nombres(int total_nodos, string nombres[]) {
    string nombre;

    for (int i=0;i<total_nodos;i++) {
        cout << "\n";
        cout << "Ingrese el nombre del nodo " << i+1 << ": ";
        cin >> nombre;
        nombres[i] = nombre;
    }
}

/* Función para encontrar el indice de la matriz <<Función vestigial>> */
int matriz_grafo::buscar_matriz(int total_nodos, string dato, string nombres[]) {
    for (int i=0; i<total_nodos;i++) {
        if (nombres[i] == dato) {
            return i;
        }
    }
    return 0;
}



/* ----------------------------------------------------------- */
/* ------ Funciones para manipular la matriz usuario --------- */
/* ----------------------------------------------------------- */

/* Función para rellenar la matriz usuario según lo que indique el usuario */
void matriz_grafo::llenar_matriz(int total_nodos, string nombres[]) {
    /* Crea el atributo matriz */
    this->matriz = new int*[total_nodos];
    this->matriz_verificar = new int*[total_nodos];
    int distancia;

    /* Inicializamos la matriz */
    for (int i=0;i<total_nodos;i++) {
        this->matriz[i] = new int[total_nodos];
        this->matriz_verificar[i] = new int[total_nodos];
    }

    /* Llenar la matriz */
    for (int i=0;i<total_nodos;i++) {
        cout << "\n";
        cout << "Trabajando con el nodo: " << nombres[i];
        cout << "\n";

        for (int j=0; j<total_nodos;j++) {
            if (i != j) {
                /* Se verifica si la matriz es 0 y si no ha sido llenada, de manera que no
                se le pregunte al usuario reiteradamente por las mismas aristas */
                if (!this->matriz[i][j] && this->matriz_verificar[i][j] != 1) {
                    cout << "ingrese la distancia al nodo <" << nombres[j] << ">: ";
                    cin >> distancia;
                    
                    /* Llena la matriz de forma espejo */
                    this->matriz[i][j] = distancia;
                    this->matriz[j][i] = distancia;

                    /* Si se lleno esa parte de la matriz se guarda */
                    this->matriz_verificar[i][j] = 1;
                    this->matriz_verificar[j][i] = 1;
                }
            } else {
                /* Diagonal de la matriz */
                this->matriz[i][j] = 0;

                /* Se guarda si se lleno */
                this->matriz_verificar[i][j] = 1;
            } 
        }
    }
}

/* Función para mostrar la matriz usuario */
void matriz_grafo::mostrar_matriz(int total_nodos) {
    for (int i=0; i<total_nodos;i++) {
        for (int j=0;j<total_nodos;j++) {
            cout << this->matriz[i][j] << " ";
        }
        cout << "\n";
    }
}

/* Función para generar la imagen del grafo matriz usuario */
void matriz_grafo::crear_grafo(int total_nodos, string nombres[]) {

    ofstream fp;
            
    /* Abre archivo */
    fp.open ("grafo.txt");
    fp << "graph G {" << endl;
    fp << "graph [rankdir=LR]" << endl;
    fp << "node [style=filled fillcolor=yellow];" << endl;
    
    // Escribir en el archivo la matriz
    for (int i=0; i<total_nodos; i++) {
        for (int j=0; j<total_nodos; j++) {
            // evalua la diagonal principal.
            if (i < j) {
                if (matriz[i][j] > 0) {
                    fp << "\n" << '"' << nombres[i] << '"' << "--" << '"' << nombres[j] << '"' << " [label=" << this->matriz[i][j] << "];";
                }
            }
        }
    }
    fp << "}" << endl;

    /* Cierra archivo */
    fp.close();
                    
    /* Genera el grafo */
    system("dot -Tpng -ografo.png grafo.txt &");
            
    /* Visualiza el grafo */ 
    system("eog grafo.png &");
}



/* ----------------------------------------------------------- */
/* ------- Funciones para manipular el algoritmo Prim -------- */
/* ----------------------------------------------------------- */

/* Función para rellenar la matriz prim con ceros (inicializar el conjunto L) */
void matriz_grafo::llenar_matriz_prim(int total_nodos) {
    this->L = new int*[total_nodos];
    int distancia = 0;

    /* Inicializamos el conjunto L */
    for (int i=0;i<total_nodos;i++) {
        this->L[i] = new int[total_nodos];
    }

    /* Llenar el conjunto L con ceros */
    for (int i=0;i<total_nodos;i++) {
        for (int j=0; j<total_nodos;j++) {
                this->L[i][j] = distancia;
        }
    }
}

/* Función para mostrar la matriz Prim (conjunto L) */
void matriz_grafo::mostrar_matriz_prim(int total_nodos) {
    for (int i=0; i<total_nodos;i++) {
        for (int j=0;j<total_nodos;j++) {
            cout << this->L[i][j] << " ";
        }
        cout << "\n";
    }
}

// Función central que aplica el algoritmo de Prim
void matriz_grafo::algoritmo_Prim(int total_nodos, string nombres[]) {
    
    // Crear un vector U de tamaño N
    int U[total_nodos];

    // Inicializar el vector U en false's
    memset(U,false,sizeof(U));

    // Cambiar a true el sitio de inicio
    U[0] = true;

    cout<<"=== Conjunto L ===" << endl;
    cout<<"Arista : Distancia" << endl;
    int cnt_aristas = 0;

    // Reccorre aristas
    while(cnt_aristas < total_nodos-1) {
        //Define el min como high (999999)
        int min = HIGH;

        // define u y v
        int u = 0, v = 0;

        for(int i = 0; i < total_nodos; i++) {
            // Si es el sitio de inicio entonces
            if (U[i]){
                for(int j = 0; j < total_nodos; j++) {
                    // Si es menor al minimo entonces
                    if(min > this->matriz[i][j]) {
                        // Si el vector U en j es false entonces guardar
                        if(!U[j] && this->matriz[i][j]) {
                            min = this->matriz[i][j];
                            u = i;
                            v = j;
                        }
                    }
                }
            }
        }
        this->L[u][v] = this->matriz[u][v];
        this->L[v][u] = this->matriz[u][v];


        cout<<" "<< nombres[u] <<"-"<< nombres[v] <<": "<<this->matriz[u][v]<<endl;
        U[v] = true;
        cnt_aristas++;
    }
    cout <<"\nMatriz del algoritmo de Prim: " << endl;
    mostrar_matriz_prim(total_nodos);
    cout <<"\nCreando grafo respecto al conjunto L..." << endl;
    crear_grafo_prim(total_nodos, nombres);
}

/* Función para generar la imagen del grafo matriz del algoritmo de prim */
void matriz_grafo::crear_grafo_prim(int total_nodos, string nombres[]) {

    ofstream fp;
            
    /* Abre archivo */
    fp.open ("grafo_prim.txt");
    fp << "graph G {" << endl;
    fp << "graph [rankdir=LR]" << endl;
    fp << "node [style=filled fillcolor=green];" << endl;
    
    // Escribir en el archivo el conjunto L
    for (int i=0; i<total_nodos; i++) {
        for (int j=0; j<total_nodos; j++) {
            // evalua la diagonal principal.
            if (i < j) {
                // ignora las conexiones iguales o menores a 0
                if (this->L[i][j] > 0) {
                    fp << "\n" << '"' << nombres[i] << '"' << "--" << '"' << nombres[j] << '"' << " [label=" << this->L[i][j] << "];";
                }
            }
        }
    }
    fp << "}" << endl;

    /* Cierra archivo */
    fp.close();
                    
    /* Genera el grafo */
    system("dot -Tpng -ografo_prim.png grafo_prim.txt &");
            
    /* Visualiza el grafo */ 
    system("eog grafo_prim.png &");
}