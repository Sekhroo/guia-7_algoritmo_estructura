//#include <stdlib.h>
#include <iostream>
using namespace std;

#include "matriz_grafo.h"


/* Función para limpiar la terminal */
void clear() {
    cout << "\x1B[2J\x1B[H";
}

/* Función para el menu principal */
string menu_principal (string opt) {
    cout << "============= Menu =============" << endl;
    cout << "Aplicar algoritmo Prim _____ [1]" << endl;
    cout << "Mostrar matriz _____________ [2]" << endl;
    cout << "Visualizar el grafo ________ [3]" << endl;
    cout << "Salir del programa _________ [0]" << endl;
    cout << "================================" << endl;
    cout << "Opción: ";

    cin >> opt;

    clear();

    return opt;
}

/* Función main */
int main(int argc, char **argv){

    // Valida cantidad de parámetros mínimos.
    if (argc<2) {
		cout << "Comando mal ejecutado, el programa no se podra inciar." << endl;
		cout << "Recuerde que el programa recibe dos parametros de entrada, de la siguiente forma: " << endl;
		cout << "-> Ejemplo: ./programa 'Número mayor a 2' (sin comillas)" << endl;
    }

    // Convierte string a entero && tamano_matriz es una variable int para crear la matriz NxN.
    int tamano_matriz = atoi(argv[1]);

    // Inicializa la opción para el menu
    string option = "\0";

    // Crea y llama al objeto algorithm_prim
    matriz_grafo *algorithm_prim = new matriz_grafo();

    // Antigüo comando para preguntar al usuario el tamaño de la matriz usuario 
    /* Funciones iniciales: Generar matriz
    cout << "Ingrese el tamaño de la matriz: ";
    cin >> tamano_matriz; */

    // Inicializa un Array del tipo string con nombres de los nodos
    string nombres[tamano_matriz];

    // Genera la matriz usuario como atributo del objeto algorithm_prim junto a los nombres y el tamaño
    algorithm_prim->agregar_nombres(tamano_matriz, nombres);
    algorithm_prim->llenar_matriz(tamano_matriz, nombres);

    // Inicializa la matriz del algoritmo Prim (conjunto L)
    algorithm_prim->llenar_matriz_prim(tamano_matriz);

    clear();

    // Iterar el menu principal
    while (option != "0") {

        // Llamar menu
        option = menu_principal(option);

        // Opción para aplicar el algoritmo de Prim en la matriz
        if (option == "1") {
            algorithm_prim->algoritmo_Prim(tamano_matriz,nombres);
        
        // Opción para mostrar la matriz
        }else if (option == "2") {
            algorithm_prim->mostrar_matriz(tamano_matriz);

        // Opción para crear y visualizar el grafo con la librería graphviz
        }else if (option == "3") {
            algorithm_prim->crear_grafo(tamano_matriz, nombres);

        }
    }

    return 0;
}