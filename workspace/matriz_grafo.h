#ifndef MATRIZ_GRAFO_H
#define MATRIZ_GRAFO_H

//#include <stdlib.h>
#include <iostream>
using namespace std;

class matriz_grafo {

    private:
        // Atributo del objeto: Matriz usuario y matriz verificar
        int **matriz;

        // Matriz que ayuda a que no se reitere la conexión entre nodos
        int **matriz_verificar; // de manera que el usuario solo añade 1 vez los datos

        // Atributo del objeto: Conjunto L
        int **L;
        

    public:
        matriz_grafo();

        /* ------------------------------------------------------------ */
        /* --- Funciones para manipular los nombres de los vertices --- */
        /* ------------------------------------------------------------ */

        /* Funciones de soporte para nombrar los nodos del grafo */
        void agregar_nombres(int total_nodos, string nombres[]);

        /* Función para buscar el indice del nodo */
        int buscar_matriz(int total_nodos, string dato, string nombres[]);


        /* ----------------------------------------------------------- */
        /* ------ Funciones para manipular la matriz usuario --------- */
        /* ----------------------------------------------------------- */

        /* Funciones para inicializar la matriz usuario y la matriz verificar */
        void llenar_matriz(int total_nodos, string nombres[]);

        /* Funciones para mostrar la matriz por la terminal */
        void mostrar_matriz(int total_nodos);

        /* Función para visualizar un grafo */
        void crear_grafo(int total_nodos, string nombres[]);


        /* ----------------------------------------------------------- */
        /* ------- Funciones para manipular el algoritmo Prim -------- */
        /* ----------------------------------------------------------- */
        
        /* Función para inicializar con ceros la matriz prim */
        void llenar_matriz_prim(int total_nodos);

        /* Función para mostrar matriz prim */
        void mostrar_matriz_prim(int total_nodos);

        /* Función para aplicar el algoritmo de Prim */
        void algoritmo_Prim (int total_nodos, string nombres[]);

        /* Función para visualizar un grafo prim*/
        void crear_grafo_prim(int total_nodos, string nombres[]);

};
#endif