/*
Fuente de apoyo: https://www.youtube.com/watch?v=YfnDKFm6GHM
Por: ThinkX Academy
*/

#include<iostream>
#include<cstring>

using namespace std;

#define N 5
#define HIGH 999999

int main(){

    // Matriz NxN
    int matriz[N][N] = {
    /*       a b c d e */
    /* a */ {0,5,0,0,5},
    /* b */ {1,0,0,3,0},
    /* c */ {0,2,0,7,0},
    /* d */ {3,0,1,0,3},
    /* e */ {5,0,0,7,0}
    };

    // Array that takes care of visited vertices
    int U[N];

    // Initialize all positions as false initially
    memset(U,false,sizeof(U));

    // Initializing v_array[0] as true to start with the 0th vertex
    U[0] = true;

    cout<<"EDGE : WEIGHT" << endl;
    int cnt_aristas = 0;

    while(cnt_aristas < N-1) {
        int min = HIGH;

        int u = 0, v = 0;

        for(int i = 0; i < N; i++) {
            if (U[i]){
                for(int j = 0; j < N; j++) {
                    if(min > matriz[i][j]) {
                        if(!U[j] && matriz[i][j]) {
                            min = matriz[i][j];
                            u = i;
                            v = j;
                        }
                    }
                }
            }
        }         
        cout<<" "<< u <<"-"<<v<<":"<<matriz[u][v]<<endl;
        U[v] = true;
        cnt_aristas++;
    }
    return 0;
}